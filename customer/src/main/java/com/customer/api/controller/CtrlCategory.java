package com.customer.api.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.customer.api.entity.Category;
import com.customer.api.entity.Region;

@RestController
@RequestMapping("/category")

public class CtrlCategory {

	Category category1 = new Category(0, "Electronica", 1);
	Category category2 = new Category(1, "Linea Blanca", 1);
	ArrayList<Category> categories = new ArrayList<Category>() {
		{ // No encontre otra manera para que el arryay
			add(category1); // funcionara como una lista dinámica
			add(category2);
		}
	};

	@GetMapping
	public ResponseEntity<List<Category>> getCategories() { // La lista es dinámica para buscar, actualizar y borrar, se
															// pueden ver los
		return new ResponseEntity<>(categories, HttpStatus.OK); // Cambios hechos si vuelves a usar getCategories()
	} // Para agregar no funciona dinamicamente porque contains() no checa atributo
		// por atributo
		// Sería como encadenar ands en un for para checar si la categoría existe

	@GetMapping("/{category_id}")
	public ResponseEntity<?> getCategory(@PathVariable int category_id) {

		if (category_id < categories.size() && category_id >= 0) {
			return new ResponseEntity<>(categories.get(category_id), HttpStatus.OK);
		}

		return new ResponseEntity<>("Id no encontrado", HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<String> createCategory(@Valid @RequestBody Category category, BindingResult bindingResult) {
		String message = "";

		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().get(0).getDefaultMessage();
			return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
		}

		if (!category.getCategory().equals(category1.getCategory())
				&& !category.getCategory().equals(category2.getCategory())) {
			message = "category created";
			categories.add(category);
			return new ResponseEntity<>(message, HttpStatus.OK);
		}
		message = "category already exist";
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

	@PutMapping("/{category_id}")
	public ResponseEntity<String> updateCategory(@PathVariable int category_id, @Valid @RequestBody Category category,
			BindingResult bindingResult) {

		String message = "";
		if (bindingResult.hasErrors()) {
			bindingResult.getAllErrors().get(0).getDefaultMessage();
			return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
		}

		if (category_id < categories.size() && category_id >= 0) {
			categories.set(category_id, category);
			return new ResponseEntity<>("Category updated", HttpStatus.OK);

		}
		return new ResponseEntity<>("Id not found", HttpStatus.OK);

	}

	@DeleteMapping("/{category_id}")
	public ResponseEntity<String> deleteCategory(@PathVariable int category_id) {

		if (category_id < categories.size() && category_id >= 0) {
			categories.remove(category_id);
			return new ResponseEntity<>("Category removed", HttpStatus.OK);
		}

		return new ResponseEntity<>("Id not found ", HttpStatus.OK);
	}

}
